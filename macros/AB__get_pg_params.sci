//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [color_id, traj_color_id, thickness, traj_thickness, steps, area, res, coord]=AB__get_pg_params(block)

    thickness = block.ipar(1);
    traj_thickness = block.ipar(2);
    color_id = block.ipar(3);
    traj_color_id = block.ipar(4);
    steps = block.opar(1);
    area = block.opar(2); 
    res = block.opar(3); 
    coord = block.opar(4);

funcprot(previousprot)
endfunction
