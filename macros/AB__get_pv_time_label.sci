//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [label] = AB__get_pv_time_label(inptr, block_name)

    siz = size(inptr);
    time_size = max(siz);
    select time_size
      case 1 then
	label = string(inptr);
      case 6 then
        // check input correctness
        time = getscicosvars("t0");
        err = AB_check_cal_date(inptr, time, block_label);
        if err < 0 then
 	  set_blockerror(err);
        elseif err > 0 then
	  label = sprintf("%d:%02d", inptr(4), inptr(5));
        end
      else
	label = "Incorrect time input";
    end

endfunction
