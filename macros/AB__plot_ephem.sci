//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [err] = AB__plot_ephem(win_id, pos_sph, traj_color_id, thickness, data_bounds)
    err = 1;
    if ~exists('win_id','local') then  
	err = -1; 
    else
	scf(win_id);

	// plot
	// immediate_drawing_save = f.immediate_drawing; // store field
	// f.immediate_drawing = "off"; 

	// interval in longitude containing the view
	bmin = (data_bounds(1,1)+data_bounds(2,1))/2 - 180; 
	bmax = (data_bounds(1,1)+data_bounds(2,1))/2 + 180; 
	box = [data_bounds(1,1), data_bounds(1,2), data_bounds(2,1),data_bounds(2,2)];

	// plot curve (never empty)
	//[x, y] = CL__plot_genxy(pos_sph(1,:), pos_sph(2,:), bmin, bmax);
	//if ~or(isnan([x y])) & (x <= bmax) & (x >= bmin)
	//    plot2d(x, y, rect=box);
	//    h = CL_g_select(gce(), "Polyline"); 
	//    h.foreground = traj_color_id; 
	//    h.thickness = thickness; 
	//end
	a = win_id.children;
	if(size(a.children, "r") < 2)
		[x, y] = CL__plot_genxy(pos_sph(1,:), pos_sph(2,:), bmin, bmax);
		plot2d(x, y, rect=box);
		h = CL_g_select(gce(), "Polyline"); 
		h.foreground = traj_color_id; 
		h.thickness = thickness;		
	else//if ~or(isnan([x y])) & (x <= bmax) & (x >= bmin)
		pos_current = a.children(1).children.data';	// Data so far is transposed
		[x, y] = CL__plot_genxy([pos_current(1,:), pos_sph(1,:)], [pos_current(2,:), pos_sph(2,:)], bmin, bmax);
		a.children(1).children.data = [x',y'];
	end

	// adjustments (grid, databounds)
	//a = gca();
	//a.data_bounds = data_bounds;
	//a.tight_limits = "on";

	// f.immediate_drawing = immediate_drawing_save; // restore field
    end
endfunction
