//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function err=AB_check_kep(kep, time, label)
  err = 1;
  sma = kep(1);
  ecc = kep(2);
  inc = kep(3);
  if and(kep == zeros(6,1)) then
    err = 0;
    if time <> 0 then
      warning(msprintf("\t%s : Encountered empty input vector at time = %f!\n", label, time));
    end
  elseif sma <= 0 then
    err = -1;     
    messagebox("Semi-major axis must be greater than 0", label, "error"); 
  elseif ecc < 0 then
    err = -1;      
    messagebox("Eccentricity must be greater or equal 0", label, "error");
  elseif inc>%pi | inc<0  then
    err = -1;
    messagebox("Inclination must be between 0 and PI radians", label, "error");
  end
endfunction
