//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_co_ell2car(block,flag)

previousprot = funcprot(0)

block_name = "AB_co_ell2car";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
    // Output computation
    pos_ell = block.inptr(1);
    
    er = block.rpar(1);   // Equatorial radius of reference ellipsoid [m] 
    obla = block.rpar(2); // Oblateness of reference ellipsoid
        
    [pos_car ,jacob] = CL_co_ell2car(pos_ell,er,obla);

    // Set block outputs
    block.outptr(1) = pos_car;
    block.outptr(2) = jacob; 
    
  case 4 then
    // Initialization
    AB_check_param_number(block_name, block, 0, 0, 4);
  case 5 then
    // Ending
    ;
  case 6 then
    // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
