//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_mod_moonSun(block,flag)

previousprot = funcprot(0);

block_name = "AB_mod_moonSun";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    body = block.opar(1);
    if body == "s" then
      cjd = block.inptr(1);
      [pos_ECI,vel_ECI] = CL_eph_sun(cjd , frame="ECI", model="med");
      block.outptr(1) = pos_ECI;
    elseif body == "m" then
      cjd = block.inptr(1);
      [pos_ECI,vel_ECI] = CL_eph_moon(cjd , frame="ECI", model="med");
      block.outptr(1) = pos_ECI;
    else
      error('Body must be either s or m\n');
      set_blockerror(-1);
    end
  case 4 then
  // Initialization
    if block.nin<>1 | block.nout<>1 then
      error('Incorrect number of block inputs or output for calling CL_mod_moonSun function');
      set_blockerror(-1);
    end
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction

