//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_rot_rotVect(block,flag)

previousprot = funcprot(0)

norm_error = 0.01;			// quaternion norm = 1 tolerance
block_name = "AB_rot_rotVect";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    r_1 = block.inptr(1)(1);
    i_1 = block.inptr(1)(2);
    j_1 = block.inptr(1)(3);
    k_1 = block.inptr(1)(4);
    vec = block.inptr(2);
    quat = CL_rot_defQuat(r_1,i_1, j_1, k_1);
    n = norm(quat);
    if(or([n < 1 - norm_error, n > 1 + norm_error])) then
	error(msprintf("Quaternion norm is not equal to 1. See VECTOR_ROTATE block help for details."));
    end
    out = CL_rot_rotVect(quat, vec);
    block.outptr(1) = out;
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
