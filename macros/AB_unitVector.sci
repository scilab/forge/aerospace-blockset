//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_unitVector(block,flag)

previousprot = funcprot(0)

block_name = "AB_unitVector";
block_label = "VECTOR_NORMALIZATION";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
	input = block.inptr(1);
	siz = size(input);
	output = CL_unitVector(input);
	if or(output == ones(siz(1),siz(2))* %nan) then
            time = getscicosvars("t0");
            warning(msprintf("\t%s : One of the input vectors is empty at time = %f!\n", block_name, time));
	else
	   block.outptr(1) = output;
	end
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
