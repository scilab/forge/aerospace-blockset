//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=GROUND_STATION(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Station name';
		'Station latitude [deg]';
                'Station longitude [deg]';
                'Station altitude [m]';
                'Station mask [deg]']
      types = list( 'str', 1, 'row', 1, 'row', 1, 'row', 1, 'row', 1);
      [ok,name, lat, long, alt, mask, ..
          exprs]=scicos_getvalue("Set GROUND_STATION",labels, types, exprs);
      if ~ok then break,end
      mess=[]
      if lat<-90 | lat>90  then
        mess=[mess;'Lattitude must be between -90 and 90 degrees (positive north).';' ']
        ok=%f
      end
      if long<-180 | long>180  then
        mess=[mess;'Longitude must be between -180 and 180 degrees (positive east).';' ']
        ok=%f
      end
      if mask>=90 | mask<=-90  then
        mess=[mess;'Mask must be greater than -90 and less than 90 degrees (positive up)';' ']
        ok=%f
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end

      if ok then
        model.rpar = [lat long alt mask];
        model.opar = list(name);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end
   case 'define' then
    name = ["Houston"];
    lat =  [29.7083];
    long = [-95.3813];
    alt =  [15];
    mask = [5];

    model=scicos_model();
    model.sim=list('AB_ground_station',5)
    // no inputs
    model.in=	[];
    model.in2=	[];
    model.intyp=[];
    // one output with matrix of parameters
    model.out	 = [4];
    model.out2	 = [1];
    model.outtyp = [1];
    
    model.rpar = [lat long alt mask];
    model.opar = list(name);
    model.blocktype='d';
    model.dep_ut=[%f %t];

    exprs=[
       name;
       string(lat);
       string(long);
       string(alt);
       string(mask)
      ]

    gr_i=['txt=[''GROUND_STATION''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

