//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=KEPLERIAN_PROPAGATOR(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
   case 'define' then
    model=scicos_model()
    model.sim=list('AB_ex_kepler',5)
    // three inputs: keplerian elements, cjd epoch, current cjd time
    model.in=[6;1;1];
    model.in2=[1;1;1];
    model.intyp=[1;1;1];
    // one output with a six "double" elements
    model.out=[6]
    model.out2=[1]
    model.outtyp=[1]

    // Event input for clock signal:
    model.evtin = 1;
    
    model.blocktype='d'
    model.dep_ut=[%f %t]

    exprs=string([]);
    gr_i=['txt=[''KEPLERIAN_PROPAGATOR''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 4],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

