//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=KEP_TO_CAR(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
   case 'define' then
    model	= scicos_model();
    model.sim	= list('AB_oe_kep2car',5);
    // one input with a six keplerian "double" elements
    model.in	= [6];
    model.in2	= [-2];
    model.intyp	= [1];
    // two outputs with a thre cartesian "double" elements
    model.out	= [3;3];
    model.out2	= [-2;-2];
    model.outtyp= [1;1];
    // block parameters
    model.ipar	= [];
    model.rpar	= [];
    model.opar	= list();    

    model.blocktype='c';
    model.dep_ut=[%t %f];

    exprs=string([]);
    gr_i=['txt=[''KEP_TO_CAR''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 4],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

