//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=PLANET_MODEL(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Planet name:';
		'Model precision:';
		'Accept herited events (0/1)'];
      types = list('str', 1, 'str', 1, 'col', 1);
      [ok, planet, mod, herited, exprs]=scicos_getvalue(..
       ["Set PLANET_MODEL block parameters"; "Planets to hcoose from are: Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus or Neptune"; "Model precision must be: med, high or full"],labels, types, exprs);

      if ~ok then break,end
      mess=[]
      if and(planet <> ["Mercury" "Venus" "Earth" "Earth" "Mars" "Jupiter" "Saturn" "Uranus" "Neptune"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Planet name")), ..
          gettext("Value must be a planet name: Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus or Neptune"));
        ok=%f
      end 
      if herited<>0 & herited<>1 then
        mess=[mess;'Accept herited events must have value of either 1 or 0';' ']
	message(['Some specified values are inconsistent:';
	         ' ';mess])
        ok=%f
      end
      if and(mod <> ["med" "high" "full"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Model precision")), ..
          gettext("Value must be equal to med, high, or full."));
        ok=%f
      end

      if ok then
        [model,graphics,ok] = set_io(model,graphics,list([1 1],..
          [1]),list([3 1; 3 1],[1; 1]),ones(1-herited,1),[])
      end

      if ok then
        model.evtin= ones(1-herited,1);
	model.opar = list(planet, mod);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end

   case 'define' then
    planet = "Mars";
    mod  = "med";

    model=scicos_model();
    model.sim=list('AB_eph_planet',5);
    // one input with a single "double" element
    model.in=[1];
    model.in2=[1];
    model.intyp=[1];
    // two outputs with a 3x1 "double" elements
    model.out=[3; 3];
    model.out2=[1; 1];
    model.outtyp=[1; 1];

    // Event input for clock signal:
    model.evtin = 1;
    
    model.blocktype='c';
    model.dep_ut=[%t %f];

    model.opar = list(planet, mod);

    exprs=[ 	planet;
		mod;
		"0"];
    gr_i=['txt=[''PLANET_MODEL''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

