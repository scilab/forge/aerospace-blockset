//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=SUN_MODEL(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Accept herited events (0/1)'];
      types = list('col', 1);
      [ok, herited, exprs]=scicos_getvalue(..
       "Set SUN_MODEL block parameters",labels, types, exprs);

      if ~ok then break,end
      mess=[]
      if herited<>0 & herited<>1 then
        mess=[mess;'Accept herited events must have value of either 1 or 0';' ']
        ok=%f
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end

      if ok then
        [model,graphics,ok] = set_io(model,graphics,list([1 1],..
          [1]),list([3 1],[1]),ones(1-herited,1),[])
      end

      if ok then
        model.evtin= ones(1-herited,1);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end

   case 'define' then
    model=scicos_model()
    model.sim=list('AB_mod_moonSun',5)
    // two inputs with a single "double" element
    model.in=[1];
    model.intyp=[1];
    // one output with a single "double" element
    model.out=[3];
    model.outtyp=[1];

    // Event input for clock signal:
    model.evtin = 1;
    
    model.blocktype='c';
    model.dep_ut=[%t %f];

    model.opar = list("s");

    exprs=string(["0"]);
    gr_i=['txt=[''SUN_MODEL''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];
    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

