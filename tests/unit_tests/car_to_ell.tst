// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for car_to_ell tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/car_to_ell.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  car_in : cartesian position
//  ell_out : elliptical position output vectors (to_workspace)
//  jacob_out : jacobian of the transformation (to_workspace)

car_in=[    0.5;
            1;
            1.5];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_pos = [ 1.1071487  
            1.5707702  
           -6356750.1 ];
exp_jacob = [   -0.8         0.4        0
                -0.0000104  -0.0000209  6.091D-10
                 0.0000117   0.0000233  1];

//Validate results
assert_checkequal(exp_time, pos_out.time);
assert_checkequal(exp_time, jacob_out.time);
assert_checkalmostequal(exp_pos', pos_out.values(1,:),1e-4);
assert_checkalmostequal(exp_pos', pos_out.values(2,:),1e-4);
assert_checkalmostequal(exp_pos', pos_out.values(3,:),1e-4);
assert_checkalmostequal(exp_jacob, jacob_out.values(:,:,1),1e-2);
assert_checkalmostequal(exp_jacob, jacob_out.values(:,:,2),1e-2);
assert_checkalmostequal(exp_jacob, jacob_out.values(:,:,3),1e-2);

