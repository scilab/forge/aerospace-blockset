// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for cjd_time tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/cjd_time.zcos");
assert_checktrue(result);

// Define input values
multiplier = 1;
year = 2010;
month = 1;
day = 1;
hour = 0;
minute = 0;
second = 0;

// Define Expected values
values_exp = [21915.1; 21915.2; 21915.3; 21915.4; 21915.5; 21915.6; 21915.7; 21915.8; 21915.9];
time_exp = [0.1; 0.2; 0.3; 0.4; 0.5; 0.6; 0.7; 0.8; 0.9];


// Clear variables used for outputs
clear('cjd');

// Simulate
scicos_simulate(scs_m);

//Validate results
assert_checkequal(cjd.values, values_exp);
assert_checkalmostequal(cjd.time, time_exp, 1E-8);
