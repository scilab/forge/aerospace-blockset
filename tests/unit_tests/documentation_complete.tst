// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

[macros,base_path] = libraryinfo('xcos_aerolib');
s = filesep();


// Locate help files
help_files = findfiles(base_path + '..' + s + 'help' + s + 'en_US' + s + 'blocks', '*.xml');
number_of_help = size(help_files);
help_files = strsubst(help_files,'.xml','')
help_files = gsort(help_files);

// Locate macro files
macro_files = findfiles(base_path + '..' + s + 'macros', '*.sci');
number_of_macro = size(macro_files);
macro_files = strsubst(macro_files,'.sci','')
macro_files = gsort(macro_files);

// the block files and help files
block_files = [];
for i =1:number_of_macro(1)
	if(~strcmp(macro_files(i), convstr(macro_files(i), 'u')));
		block_files = [block_files; macro_files(i)];
	end
end
number_of_block = size(block_files);
block_files = gsort(block_files);


// Check if all of the blocks are documented
assert_checktrue(and(block_files == help_files));
