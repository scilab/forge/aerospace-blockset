// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for car_to_ell tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/ell_to_car.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  ell_in : elliptical position
//  car_out : cartesian position output vectors (to_workspace)
//  jacob_out : jacobian of the transformation (to_workspace)

ell_in=   [ 0.21162117180431245;
            1.88844625065786453;
            110. ];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_pos = [ -1953617.59591063065  
            -419711.009606912441  
            6036856.73927161563  ];
exp_jacob = [    419711.009606912441,   -5938057.71355757304,   -0.30536725130628495;
                -1953617.59591063065,   -1275719.56931502442,   -0.06560444460314516;
                 0.,                    -1996881.10071963165,    0.94997205152465258  ];
  

//Validate results
assert_checkequal(exp_time, car_out.time);
assert_checkequal(exp_time, jacob_out.time);
assert_checkalmostequal(exp_pos', car_out.values(1,:),1e-6);
assert_checkalmostequal(exp_pos', car_out.values(2,:),1e-6);
assert_checkalmostequal(exp_pos', car_out.values(3,:),1e-6);
assert_checkalmostequal(exp_jacob, jacob_out.values(:,:,1),1e-8);
assert_checkalmostequal(exp_jacob, jacob_out.values(:,:,2),1e-8);
assert_checkalmostequal(exp_jacob, jacob_out.values(:,:,3),1e-8);

