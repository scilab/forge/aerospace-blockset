// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for quat_input tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/quat_input.xcos");
assert_checktrue(result);

//This diagram uses 8 variables : 
//    r_in - Semi-major axis [m] (input value)
//    i_in - Eccentricity (input value)
//    j_in - Inclination [rad] (input value)
//    k_in - Periapsis argument [rad] (input value)
//    r_out - Semi-major axis [m] (output value)
//    i_out - Eccentricity (output value)
//    j_out - Inclination [rad] (output value)
//    k_out - Periapsis argument [rad] (output value)


// Define input values
r_in = 9150600;
i_in = 0.1;
j_in = 26.126875;
k_in = 19.767044;

//Define expected values
r_exp = 9150600;
i_exp = 0.1;
j_exp = 26.126875;
k_exp = 19.767044;

scicos_simulate(scs_m);
values = [r_out.values; i_out.values; j_out.values; k_out.values];
time = [r_out.time; i_out.time; j_out.time; k_out.time];

// Define expected values
expected = [r_exp; i_exp; j_exp; k_exp];
exp_time = [0.1; 0.1; 0.1; 0.1];

//Validate results
assert_checkalmostequal(values, expected,1e-6);
assert_checkequal(time, exp_time);
