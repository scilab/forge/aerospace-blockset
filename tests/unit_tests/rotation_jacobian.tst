// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test data is generated by manualy calculating jacobian

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for rotation_jacobian tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/rotation_jacobian.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  quat_in - input quaternion [1x4] vector
//  vec_in  - input vector [1x3] vector
//  J_out   - output jacobian [3x4] matrix

quat_in = [1; 2; 3; 4];
vec_in = [1; 2; 3];

// normalize vector and jacobian
quat_in = quat_in / norm(quat_in);
vec_in = vec_in / norm(vec_in);

scicos_simulate(scs_m);

// Define expected values
time_expected = [0.1; 1.1; 2.1];
J_expected(:,:,1) = [  -0.0975900,   1.7566201,  -0.4879500,   1.110D-16;  
                        0.1951800,  -0.1951800,   1.3662601,  -0.7807201;  
                       -0.0975900,  -0.9759001,  -0.8783101,   0.7807201 ];
J_expected(:,:,2) = [  -0.0975900,   1.7566201,  -0.4879500,   1.110D-16;  
                        0.1951800,  -0.1951800,   1.3662601,  -0.7807201;  
                       -0.0975900,  -0.9759001,  -0.8783101,   0.7807201 ];

J_expected(:,:,3) = [  -0.0975900,   1.7566201,  -0.4879500,   1.110D-16;  
                        0.1951800,  -0.1951800,   1.3662601,  -0.7807201;  
                       -0.0975900,  -0.9759001,  -0.8783101,   0.7807201 ];


//Validate results
assert_checkequal(J_out.time, time_expected);
assert_checkalmostequal(J_out.values, J_expected, 1e-3);
