// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

// Following scilab code wasu used to generate the test data:
//
//	pos_sat_in = [7000.e3; 0; 0]; // ECI
//	pos_sun_in = CL_eph_sun(CL_dat_cal2cjd(2000,3,21)); // ECI
//	cp_in = 1.5
//	A_in = 10
//	F_expected = CL_fo_srpAcc(pos_sat_in, pos_sun_in, cp_in * A_in, %f)

//Load xcos test diagram for atm_drag_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/sol_drag_model.zcos");
assert_checktrue(result);

//This diagram uses variables : 
// pos_sat_in	- velocity vector
// pos_sun_in	- atmsphere density
// cp_in	- reflectivity coefficient
// A_in		- surface area
// F_out	- force vector

// Define input values
pos_sat_in	= [7000.e3; 0; 0]
pos_sun_in	= [	149017200247.13562;
			1644628372.3832972;
			711923805.953390479];
cp_in		= [	1.5];
A_in		= [	10];


scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
F_expected  =[	-0.00006893318219062,	-0.00000076081682356,	-0.00000032934103397;  
		-0.00006893318219062,	-0.00000076081682356,	-0.00000032934103397;  
		-0.00006893318219062,	-0.00000076081682356,	-0.00000032934103397];

//Validate results
assert_checkequal(F_out.time, exp_time);
assert_checkalmostequal(F_out.values, F_expected, 1e-7);

